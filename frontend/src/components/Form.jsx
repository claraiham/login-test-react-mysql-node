import { useState} from "react"
import './Form.css'
import { Link, useNavigate } from "react-router-dom"

function Form() {
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const [errorEmail, setErrorEmail] = useState('')

    const [data, setData] = useState(null)

    const navigate = useNavigate();

    const onSubmit = (event) => {
      event.preventDefault()



      // appel api en dehors d'un useEffect, car réponse à l'action utilisateur (soumission de formulaire), appel api qui se déclenceh grâce à l'envoie du formul
  
      fetch('/api/login', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ email, password }) // Utilisez les valeurs actuelles de l'état
      })
      .then((res) => res.json())
      .then((data) => {
          setData(data)

          if (data.success) {
            // Utilisez history.push pour effectuer une redirection programmation
            navigate(`/Connected/${data.id}`)
          }



  //vérification de l'email ( adresse email qui contient @ etc)
          function isEmailValid(email){
              let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            
              if (!email.match(re)){
                  setErrorEmail("Adresse email invalide")
              } else {
                  setErrorEmail("")
              }
          }
          isEmailValid(email);
      })
      .catch((error) => {
          console.error('Erreur lors de la requête POST:', error)
      });
  }

  return (
    <div className="login-content">
      {/* <p>{data ? data : "Loading..." }</p> */}
      {/* data a une valeur ? affiche ce qui est dans data, sinon affiche "Loading..."*/}

      {/* <p>{data ? (data.success ? <Link key={r.id} to={"Connected"}/> : "Email ou mot de passe invalide") : "Echec de la connexion"}</p> */}
      {/* à faire : 
      -afficher "echec de la connexion seulement si la connexion à la bdd n'est pas établie"
      -ajout d'un router pour afficher une page lorsqu'on est connecté */}
      {data && data.success && data.id !== undefined ? (
  <Link key={data.id} to={`/Connected/${data.id}`} />
) : (
  <p>Email ou mot de passe invalide</p>
)}



        <form onSubmit={onSubmit} className = "form">
            <label htmlFor="email">E-mail
            <input type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
            {errorEmail}
            </label>
            <label htmlFor="password">Mot de passe
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
            </label>
            <button onClick={onSubmit}>Submit</button>
        </form>
    </div>
  )
}

export default Form