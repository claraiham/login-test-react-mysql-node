const express = require("express");
const mysql = require('mysql2/promise');
const app = express();
// utiliser bcrypt pour hacher les mdp

const cors = require("cors"); // permet d'activer CORS dans une application Node.js avec Express, autorisant ainsi les requêtes HTTP provenant de domaines différents.

const PORT = 3001;


app.use(express.json());
app.use(cors());

// app.get("/api", (req, res) => {
//     res.json({ message: "Hello from server!" });
//   }); // exemple de modèle de création d'API


// nécessaire pour la connexion à la BDD
const dbConfig = {
    host: 'localhost',
    user: 'root',// mon super identifiant
    password: 'root', // mon mdp hyper confidentiel et hyper sécurisé
    database: 'usersdb', // nom de la bae de données à laquelle je me connecte
  };
  
  // Création d'une connexion MySQL
  const pool = mysql.createPool(dbConfig);
  
  // Connexion à la base de données
    pool.getConnection()
    .then((connection)=>{

      console.log("Connecté à la base de données MySQL!");
  
    app.post('/api/login', async (req, res) => {
        console.log('Route /api/login atteinte'); 
        
      const { email, password } = req.body;//Les données du corps de la requête sont extraites pour être utilisées dans la requête SQL d'authentification.
  
      const query = `SELECT * FROM users WHERE email = ? AND password = ?`; // Requête SQL pour vérifier les identifiants
  
      try {
        // Exécution de la requête avec les données fournies
        const [results] = await pool.execute(query, [email, password]);
  
        // Vérification des résultats de la requête
        if (results.length > 0) {
          res.json({ success: true, message: 'Vous êtes connecté' }); // identifiant et mot de passe correct
        } else {
          res.status(401).json({ success: false, message: 'Email ou mot de passe invalide' });// identifiant et ou mot de passe incorrect
        }
      } catch (err) {
        console.error('Erreur lors de la requête SQL:', err);
        res.status(500).json({ success: false, message: 'Erreur de requête SQL' });// Erreur lors de l'exécution de la requête SQL
      }
      finally {
        connection.release(); // Libérer la connexion après utilisation
    }
    });

    });


// Démarrage du serveur sur le port spécifié
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});


